
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => MapState();

}

class MapState extends State<MapScreen>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 160,
        title: const Text('Login', style: TextStyle(color: Colors.yellow),),
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.red,
                Colors.yellow,
                Colors.green
              ]
           ) ,
          ),
        ),
      ),

    body: GoogleMap(
      initialCameraPosition: CameraPosition(
        target: LatLng(10,100),
        zoom: 12
      ),
    ),
    );
  }

}