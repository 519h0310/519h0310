import 'package:flutter/material.dart';
import 'package:googlemap_package/view/MapScreen.dart';

void main() {
  runApp(App());
}

class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Google Map',
      routes: {
        '/map': (context) => MapScreen()
      },
      initialRoute: '/map',
    );
  }

}

