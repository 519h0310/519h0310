import 'dart:convert';

import "package:flutter/material.dart";

import '../module/Pageview.dart';

class StreamIndex {
  Stream<int> getIndex() async*{

    yield* Stream.periodic(const Duration(seconds: 15),(int t){
      int index = t%5;
      return index;
    });
  }
}