
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:soundio/firebase/firebase_auth.dart';
import 'package:soundio/view/HomeScreen.dart';
import 'package:soundio/view/LoginScreen.dart';
import 'package:soundio/view/WelcomScreen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';

Future <void> main() async{

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(

        providers: [
          Provider<FirebaseAuthentication>(create: (_) => FirebaseAuthentication(FirebaseAuth.instance),
          ),
          StreamProvider(create: (context) => context.read<FirebaseAuthentication>().authStateChanges, initialData: null,
          )
        ],
        child: MaterialApp(
          color: Colors.amber,
          debugShowCheckedModeBanner: false,
          title: 'Soundio',
          theme: ThemeData(
            primaryColor: Colors.black
          ),
          home: WelcomeScreen(),
        )
      );
  }
}

class Start extends StatelessWidget{
  const Start({Key? key}) : super (key: key);


  @override
  Widget build(BuildContext context) {
    final firebaseAuth = context.watch<User>();

    if(firebaseAuth != null){
      return Home(user: 'Khách');
    }
    return const LoginScreen();
  }}

