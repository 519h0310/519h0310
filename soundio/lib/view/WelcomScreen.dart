import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:lottie/lottie.dart';
import 'package:soundio/routes/routeScreen.dart';

import 'HomeScreen.dart';

class WelcomeScreen extends StatefulWidget{


  @override
  State<StatefulWidget> createState() => WelcomeState();
}

class WelcomeState extends State<WelcomeScreen> with SingleTickerProviderStateMixin {
  String user = 'Khách';
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          const Padding(
            padding: EdgeInsets.all(25.0),
          ),
          Stack(
            alignment: Alignment.center,
            children: [
              Container(
                width: 400.0,
                height: 280.0,
                child: Lottie.asset('asset/images/rainbow.json')

              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(40.0),
                child: Image(image: AssetImage('asset/images/person.png'))
              )

             ],
          ),
          
          const Image(
              image: AssetImage('asset/images/text_title.png')),

          const Padding(
            padding: EdgeInsets.all(10.0),),

          const Text(
            "Nền tảng âm thanh với nội dung phong phú và chất lượng cao.",
            style:  TextStyle(
                decoration:TextDecoration.none,
                color: Colors.black,
                fontSize: 10.0),

            textAlign: TextAlign.center,

          ),

          const Padding(
            padding: EdgeInsets.all(20.0),
          ),

          SizedBox(
            width: 380.0,
            height: 50.0,

            child: TextButton(
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Home(
                        user: user,
                      )
                    ),
                  );
                },
                child: const Text('TRẢI NGHIỆM NGAY',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 15.0),
                ),
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.orange[800]),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)
                      )
                  ),
                ),
            ),
          ),

          const Padding(
            padding: EdgeInsets.all(10.0),
          ),

          SizedBox(
            width: 380.0,
            height: 50.0,

            child: TextButton(
              onPressed: (){
                WelcomToLogin(context);
              },
              child: const Text('ĐĂNG NHẬP',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 15.0),
              ),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.grey[200]),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)
                    )
                ),
              ),
            ),
          ),

          const Padding(
            padding: EdgeInsets.all(10.0),
          ),

          SizedBox(
            width: 380.0,
            height: 50.0,

            child: TextButton(
              onPressed: (){WelcomToRegister(context);},
              child: const Text('ĐĂNG KÝ',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 15.0),
              ),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.grey[200]),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)
                  )
                ),
              ),
            ),
          ),




        ],
      ),
    );
  }

}

