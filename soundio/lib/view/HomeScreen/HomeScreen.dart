
import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:soundio/module/Pageview.dart';

import '../../stream/StreamIndex.dart';

class HomeScreen extends StatefulWidget{
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => HomeState();

}

class HomeState extends State<HomeScreen>{

  var _currentPage = 0;
  final PageController controller = PageController();
  @override
  void initState() {
      super.initState();
      Timer.periodic(const Duration(seconds: 10), (Timer timer) {
          if(_currentPage >= 4){_currentPage = controller.initialPage;}
          else {
            _currentPage++;
          }

          if(controller.hasClients) {
            controller.animateToPage(_currentPage, duration: const Duration(seconds: 1), curve: Curves.easeOut);
          }

      });
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: Column(
            children: [
              Container(
                color: Colors.limeAccent.withOpacity(0.1),
                padding: const EdgeInsets.only(left: 20.0, right: 20.0,top: 15.0),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: const [
                    Text('Chào Khách',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 30,
                      ),
                      textAlign: TextAlign.start,
                    ),
                    Align(
                      child: Icon(Icons.search_sharp),
                    )
                ],
                )
              ),

              TextButton(
                onPressed: (){},
                child: Container(
                  width: 550,
                  height: 325,
                  padding: EdgeInsets.only(top: 25.0),
                  color: Colors.limeAccent.withOpacity(0.1),
                  child: PageView.builder(
                    controller: controller,
                    scrollDirection: Axis.horizontal,
                    onPageChanged: (index){
                      _currentPage = index;
                    },
                    itemCount: 5,
                    itemBuilder: (context, index){
                      return PageViewBuilder(index);
                    }
                )
            ),
              ),
              SmoothPageIndicator(
                    controller: controller,
                    count: 5,
                    effect: ExpandingDotsEffect(
                      dotHeight: 8,
                      dotWidth: 8,
                      activeDotColor: Colors.orange,
                      dotColor: Colors.grey.withOpacity(0.2),
                    ),
                    onDotClicked: (index)=> controller.animateToPage(index, duration: Duration(milliseconds: 500), curve: Curves.ease),
                ),

              Container(
                padding: EdgeInsets.only(top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children:[
                    Align(
                      child: TextButton(
                        onPressed: (){},
                        child: Container(
                          width: 210,
                          height: 80,
                          margin: const EdgeInsets.only( left: 5),
                          decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(8)),
                              color:Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xFFe8e8e8),
                                    blurRadius: 5.0,
                                    offset: Offset(0, 5)
                                ),
                                BoxShadow(
                                    color: Colors.white,
                                    offset: Offset(0, 0)
                                ),
                                BoxShadow(
                                    color: Colors.white,
                                    offset: Offset(0, 0)
                                )
                              ]
                          ),
                          child: Container(
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                Row(
                                  children: [
                                    Container(
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(

                                    ),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Padding(padding: EdgeInsets.only(left: 15)),
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [

                                            Text('Podcast', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),textAlign: TextAlign.start,),
                                            Text('100+ tập mới\n mỗi ngày',style: TextStyle(color: Colors.grey)),
                                          ],
                                        ),
                                        SizedBox(width: 110.0, height: 80 ,child: Image(image: AssetImage('asset/images/Podcast.jpg'), alignment: Alignment.centerRight,))
                                            ],
                                          ),
                                        ),
                                      ]
                                    )
                                  ]
                                )
                              ),
                            ),
                      )
                        ),
                    Align(
                        child: TextButton(
                          onPressed: (){},
                          child: Container(
                            width: 210,
                            height: 80,
                            margin: const EdgeInsets.only( left: 5),
                            decoration: const BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(8)),
                                color:Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xFFe8e8e8),
                                      blurRadius: 5.0,
                                      offset: Offset(0, 5)
                                  ),
                                  BoxShadow(
                                      color: Colors.white,
                                      offset: Offset(0, 0)
                                  ),
                                  BoxShadow(
                                      color: Colors.white,
                                      offset: Offset(0, 0)
                                  )
                                ]
                            ),
                            child: Container(
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Row(
                                          children: [
                                            Container(
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(

                                              ),
                                              child: Row(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  Padding(padding: EdgeInsets.only(left: 15)),
                                                  Column(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [

                                                      Text('Sách truyện', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),textAlign: TextAlign.start,),
                                                      Text('100+ sách truyện\n bản quyền',style: TextStyle(color: Colors.grey)),
                                                    ],
                                                  ),
                                                  SizedBox(width: 86.0, height: 80 ,child: Image(image: AssetImage('asset/images/Sach_Truyen.jpg'), alignment: Alignment.centerRight,))
                                                ],
                                              ),
                                            ),
                                          ]
                                      )
                                    ]
                                )
                            ),
                          ),
                        )
                    ),

                       ]
                      ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(padding: EdgeInsets.only(left: 10)),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children:[
                        Align(
                            child: TextButton(
                              onPressed: (){},
                              child: Container(
                                width: 200,
                                height: 40,
                                margin: const EdgeInsets.only( left: 5),
                                decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(8)),
                                    color:Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color(0xFFe8e8e8),
                                          blurRadius: 5.0,
                                          offset: Offset(0, 5)
                                      ),
                                      BoxShadow(
                                          color: Colors.white,
                                          offset: Offset(0, 0)
                                      ),
                                      BoxShadow(
                                          color: Colors.white,
                                          offset: Offset(0, 0)
                                      )
                                    ]
                                ),
                                child: Container(
                                    margin: EdgeInsets.only(left: 10),
                                    child: Row(
                                        children: [
                                          Text('Soundio Mood', textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),),
                                          Padding(padding: EdgeInsets.only(left: 45)),
                                          SizedBox(width: 50, height: 40 ,child: Image(image: AssetImage('asset/images/SoundioMood.jpg'),width: 200, height: 80, alignment: Alignment.centerRight,),)]
                                    )
                                ),
                              ),
                            )
                        ),
                      ]
                  ),
                  Align(
                      child: TextButton(
                        onPressed: (){},
                        child: Container(
                          width: 140,
                          height:40,
                          margin: const EdgeInsets.only( left: 5),
                          decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(8)),
                              color:Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xFFe8e8e8),
                                    blurRadius: 5.0,
                                    offset: Offset(0, 5)
                                ),
                                BoxShadow(
                                    color: Colors.white,
                                    offset: Offset(0, 0)
                                ),
                                BoxShadow(
                                    color: Colors.white,
                                    offset: Offset(0, 0)
                                )
                              ]
                          ),
                          child: Container(
                            margin: EdgeInsets.only(left: 10),
                            child: Row(
                              children: [
                                Text('Live Radio', textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),),
                                Padding(padding: EdgeInsets.only(left: 20)),
                                SizedBox(width: 40, height: 40 ,child: Image(image: AssetImage('asset/images/LiveRadio.jpg'),width: 200, height: 100, alignment: Alignment.centerRight,),)
                              ]
                            ),
                          ),
                        )
                      )
                  ),
                ],
              ),
                  ]
     )
    )
    );
  }
}

class PageViewBuilder extends StatelessWidget{
  final int index;
  PageViewBuilder(this.index);

  List<Item> list = [
    Item(image: 'https://www.linkpicture.com/q/DemNayAiChet.jpg', title: "Đêm Nay Ai Chết?", subtitle: "Kịch truyền thanh kinh dị", index: 0.0),
    Item(image: 'https://www.linkpicture.com/q/HomNayManKeGi_1.jpg', title: "Mình Đang Sống Cuộc Đời Của Ai?", subtitle: "Hành trình tìm đến tự do, hạnh phúc\ncủa một người trẻ", index: 1.0),
    Item(image: 'https://www.linkpicture.com/q/ViMatEmBiecMaTaMeLong_1.jpg', title: "Truyện Audio", subtitle: "Truyện tình yêu nam x nam", index: 2.0),
    Item(image: 'https://www.linkpicture.com/q/DapChanNamNgheTunKe.jpg', title: "Đắp Chăn Nằm Nghe Tun Kể", subtitle: "Cảm giác bình yên và thư giãn khi\nnằm nghe Tun kể", index: 3.0),
    Item(image: 'https://www.linkpicture.com/q/TraiHoaDo_1.jpg', title: "Truyện Audio", subtitle: "Kịch truyền thanh sắp ra mắt", index: 4.0),

  ];


  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            color: Colors.limeAccent.withOpacity(0.1),
            child: SizedBox(child: Image.network(list[index].image)),
            margin: const EdgeInsets.only(right: 18, left: 18),
          ),
          Container(
            color: Colors.white,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Align(
                    child: Container(
                      width: 450,
                      margin: const EdgeInsets.only( left: 22, right: 22),
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(8.0), bottomRight: Radius.circular(8.0)),
                          color:Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Color(0xFFe8e8e8),
                                blurRadius: 5.0,
                                offset: Offset(0, 5)
                            ),
                            BoxShadow(
                                color: Colors.white,
                                offset: Offset(0, 0)
                            ),
                            BoxShadow(
                                color: Colors.white,
                                offset: Offset(0, 0)
                            )
                          ]
                      ),
                      child: Container(
                        child: Row(
                          children: [
                            SizedBox(
                              child: Image.network(list[index].image, width: 100, height: 70,),
                            ),
                            Padding(padding: EdgeInsets.only(left: 10)),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(list[index].title, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),),
                                Text(list[index].subtitle,style: TextStyle(color: Colors.grey),softWrap: true,)
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ]
            ),
          ),
        ],
      ),
    );
  }

}


