import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'HomeScreen/HomeScreen.dart';
import 'HomeScreen/Discovery.dart';
import 'HomeScreen/Library.dart';
import 'HomeScreen/User.dart';

class Home extends StatefulWidget{
  const Home({Key? key, required String user}) : super(key: key);

  @override
  State<StatefulWidget> createState() => HomeState();

}

class HomeState extends State<Home>{
  bool isLogin = false;
  String nameUser = 'khách';
  int current_index = 0;

  List screens = [
    HomeScreen(),
    Discovery(),
    Library(),
    UserScreen()
  ] ;

  @override
  Widget build(BuildContext context) {

        return Scaffold(
            body: screens[current_index],
            bottomNavigationBar: BottomNavigationBar(
              iconSize: 25,
              currentIndex: current_index,
              type: BottomNavigationBarType.fixed,
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                    icon: Icon(Icons.home_outlined, color: Colors.grey,),
                    label: 'Trang chủ',
                    activeIcon: Icon(Icons.home_outlined, color: Colors.orange)
                    ),

                  BottomNavigationBarItem(
                    icon: Icon(Icons.arrow_drop_down_circle_outlined, color: Colors.grey),
                    label: 'Khám phá',
                    activeIcon: Icon(Icons.arrow_drop_down_circle_outlined, color: Colors.orange)
                    ),

                  BottomNavigationBarItem(
                    icon: Icon(Icons.folder_outlined, color: Colors.grey),
                    label: 'Thư viện',
                    activeIcon: Icon(Icons.folder_outlined, color: Colors.orange),

                  ),

                  BottomNavigationBarItem(
                    icon: Icon(Icons.person_pin_circle_outlined, color: Colors.grey),
                    label: 'Tài khoản',
                    activeIcon: Icon(Icons.person_pin_circle_outlined, color: Colors.orange),
                  ),
                ],
                onTap: (index){
                    setState(() {
                      current_index = index;
                    });
                },
                fixedColor: Colors.black,
              )
        );
  }

  PreferredSizeWidget appbar() {
    return AppBar(
      title: Text('Chào ' + nameUser),
      actions: [
        IconButton(onPressed: (){}, icon: Icon(Icons.search),
         )
      ],

    );
  }




}

