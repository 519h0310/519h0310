import 'package:flutter/material.dart';
import 'package:soundio/routes/routeScreen.dart';
class RegisterScreen extends StatefulWidget{
  const RegisterScreen({Key? key}) : super(key: key);


  @override
  State<StatefulWidget> createState() {
    return RegisterState();
  }

}

class RegisterState extends State<RegisterScreen>{
  @override
  Widget build(BuildContext context) {
    final TextEditingController emailcontroller = TextEditingController();

    return Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Column(
            children: [
              const Padding(padding: EdgeInsets.all(15.0)),
              Container(
                alignment: Alignment.topLeft,
                child: IconButton(
                  onPressed: (){RegisterToWelcom(context);},
                  icon: const Icon(Icons.arrow_back),
                  color: Colors.black,
                ),
              ),

              const Padding(padding: EdgeInsets.all(5.0)),

              Container(
                alignment: Alignment.topLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                         const Text("  Đăng ký",
                          style: TextStyle(
                              fontSize: 25.0,
                              color: Colors.black,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            const Text(
                              "   Bạn đã có tài khoản?",
                              style: TextStyle(
                                fontSize: 15.0,
                                  color: Colors.black54
                              ),
                            ),
                            TextButton(
                                onPressed: (){RegisterToLogin(context);},
                                child: const Text("Đăng nhập",
                                  style: TextStyle(
                                      fontSize: 15.0,
                                      color: Colors.green
                                  ),

                                ))
                          ],
                        ),
                      ],
                    ),

                    Padding(padding: EdgeInsets.only(right: 15.0, bottom: 25.0), child:
                    Container(
                        alignment: Alignment.centerRight,
                        width: 220.0,
                        height: 70.0,
                        child: const Image(
                            image: AssetImage('asset/images/icon_soundio.png'))
                    ),
                    )
                  ],
                ),
              ),


              const Padding(padding: EdgeInsets.all(20.0)),

              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [TextFormField(
                    cursorColor: Colors.orange[800],
                    textAlign: TextAlign.left,
                    decoration: const InputDecoration(
                        labelText: '   Email',
                        labelStyle: TextStyle(
                            color: Colors.black45
                        ),
                        floatingLabelAlignment: FloatingLabelAlignment.start,
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.black12
                          ),
                        ),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.orange,
                                width: 2.0

                            )
                        )
                    )
                  ),
                    TextFormField(
                        cursorColor: Colors.orange[800],
                        textAlign: TextAlign.left,
                        decoration: const InputDecoration(
                            labelText: '   Tên',
                            labelStyle: TextStyle(
                                color: Colors.black45
                            ),
                            floatingLabelAlignment: FloatingLabelAlignment.start,
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: Colors.black12
                              ),
                            ),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.orange,
                                    width: 2.0

                                )
                            )
                        )
                    ),
                ],
                ),
              const Padding(padding: EdgeInsets.all(25)),

              SizedBox(
                width: 380.0,
                height: 50.0,

                child: TextButton(
                  onPressed: (){},
                  child: const Text('TIẾP TỤC',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15.0),
                  ),
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.orange[800]),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)
                        )
                    ),
                  ),
                ),
              ),
              SizedBox(
                  width: 380.0,
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const[ TextField(
                          decoration: InputDecoration(
                          border: UnderlineInputBorder(

                              borderSide: BorderSide(
                                color: Colors.black12,
                              )
                          ),
                        ),
                      ),
                      ]
                      ),
                      Column(
                        children: const  [
                          Padding(padding: EdgeInsets.fromLTRB(0,0,0,48)),
                          Text("  HOẶC  ",
                              textAlign: TextAlign.center,
                              textDirection: TextDirection.ltr,
                              style: TextStyle(color: Colors.black45,
                                  backgroundColor: Colors.white
                              )
                          ),
                        ],
                      )
                    ],
                  )
              ),

              const Padding(padding: EdgeInsets.all(10)),
              SizedBox(
                width: 380.0,
                height: 50.0,

                child: TextButton(
                  onPressed: (){},
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: const [
                        Image(
                          image: AssetImage('asset/images/google_icon.png'),),
                        Padding(padding: EdgeInsets.all(10)),
                        Text('TIẾP TỤC VỚI GOOGLE',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 15.0),
                        ),

                      ]
                  ),

                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.grey[200]),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)
                        )
                    ),
                  ),
                ),
              ),
            ],
          ),
        )
    );


  }
}