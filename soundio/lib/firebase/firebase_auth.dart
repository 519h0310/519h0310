import 'package:firebase_auth/firebase_auth.dart';

class FirebaseAuthentication {
  final FirebaseAuth firebaseAuth;

  FirebaseAuthentication(this.firebaseAuth);

  Stream<User?> get authStateChanges => firebaseAuth.authStateChanges();

  Future<String> signIn({required String username, required String password}) async{
    try{
      await firebaseAuth.signInWithEmailAndPassword(email: username, password: password);
      return "Signed in";
    } on FirebaseAuthException catch (e){
      return e.message!;
    }
  }

  Future<String> signUp({required String username, required String password}) async{
    try{
      await firebaseAuth.createUserWithEmailAndPassword(email: username, password: password);
      return "Signed up";
    } on FirebaseAuthException catch (e){
      return e.message!;
    }
  }
}