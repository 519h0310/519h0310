

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:soundio/view/LoginScreen.dart';
import 'package:soundio/view/RegisterScreen.dart';
import '../view/WelcomScreen.dart';

void WelcomToLogin(BuildContext context)=> Navigator.of(context).push(MaterialPageRoute(builder: (_) => const LoginScreen()));

void WelcomToRegister(BuildContext context)=> Navigator.of(context).push(MaterialPageRoute(builder: (_) => const RegisterScreen()));

void LoginToWelcom(BuildContext context) => Navigator.of(context).push(MaterialPageRoute(builder: (_) => WelcomeScreen()));

void RegisterToWelcom(BuildContext context) => Navigator.of(context).push(MaterialPageRoute(builder: (_) => WelcomeScreen()));

void LoginToRegister(BuildContext context) => Navigator.of(context).push(MaterialPageRoute(builder: (_) => const RegisterScreen()));

void RegisterToLogin(BuildContext context) => Navigator.of(context).push(MaterialPageRoute(builder: (_) => const LoginScreen()));