import 'package:flutter/cupertino.dart';

class Item{
  late String image;
  late String title;
  late String subtitle;
  late double index ;

  Item({required this.image, required this.title, required this.subtitle, required this.index});


}

class ViewList{
  List<Item> list = [
    Item(image: 'https://www.linkpicture.com/q/HomNayManKeGi_1.jpg', title: "Mình Đang Sống Cuộc Đời Của Ai?", subtitle: "Hành trình tìm đến tự do, hạnh phúc của một người trẻ", index: 1.0),
    Item(image: 'https://www.linkpicture.com/q/ViMatEmBiecMaTaMeLong_1.jpg', title: "Truyện Audio", subtitle: "Truyện tình yêu nam x nam", index: 2.0),
    Item(image: 'https://www.linkpicture.com/q/DapChanNamNgheTunKe.jpg', title: "Đắp Chăn Nằm Nghe Tun Kể", subtitle: "Cảm giác bình yên và thư giãn khi nằm nghe Tun kể", index: 3.0),
    Item(image: 'https://www.linkpicture.com/q/TraiHoaDo_1.jpg', title: "Truyện Audio", subtitle: "Kịch truyền thanh sắp ra mắt", index: 4.0),
    Item(image: 'https://www.linkpicture.com/q/DemNayAiChet.jpg', title: "Đêm Nay Ai Chết?", subtitle: "Kịch truyền thanh kinh dị", index: 0.0),
  ];




}