import 'package:week7_lithuyet/module/data_layout.dart';
import 'package:week7_lithuyet/repositories/InMemoriesCache.dart';
import 'package:week7_lithuyet/repositories/Repository.dart';

import '../module/food.dart';

class FoodService{
  Repository _repository = InMemoriesCache();

  Food createFood(String name){
    final model = _repository.create();
    final food = Food.fromModel(model)..name = name;
    saveFood(food);
    return food;
  }

  void saveFood(Food food){
    _repository.update(food.toModel());
  }

  void deleteFood(Food food){
    _repository.delete(food.toModel().id);
  }

  List<Food> getAllFood(){
    return _repository
          .getAll()
          .map<Food>((model) => Food.fromModel(model))
          .toList();
  }

  void addDescribe(Food food, String description){
    print('food.describes = ${food.describes}');
    print('food.describes.last = ${food.describes.length}');

    final id = (food.describes.length > 0) ? food.describes.last.id : 1;

    final describe = Describe(id: id, description: description);
    food.describes.add(describe);
    saveFood(food);
  }

  void deleteDescribe(Food food, Describe describe){
    food.describes.remove(describe);
    saveFood(food);
  }
}