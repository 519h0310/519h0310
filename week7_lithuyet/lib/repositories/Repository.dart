import 'package:flutter/foundation.dart';
class Model{
    final int id;
    final Map data;

    Model({ required this.id, this.data = const{}});
}
abstract class Repository{
    Model create();

    List<Model> getAll();

    Model? get(int id);

    int update(Model item);

    Model? delete(int id);

    void clear();
}