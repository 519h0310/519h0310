import 'dart:core';
import 'package:week7_lithuyet/module/data_layout.dart';
import 'package:week7_lithuyet/service/FoodService.dart';
import '../module/food.dart';

class FoodController{
  final services = FoodService();

  List<Food> get foods => List.unmodifiable(services.getAllFood());

  void addNewFood(String name){
    if(name.isEmpty){
      return;
    }
    name = _checkForDuplicates(foods.map((food) => food.name), name);
    services.createFood(name);
  }

  void saveFood(Food food){
    services.saveFood(food);
  }

  void deleteFood(Food food){
    services.deleteFood(food);
  }

  void createDescribe(Food food, [String? description]){
    if (description == null || description.isEmpty){
      description = 'New Description';
    }

    description = _checkForDuplicates(food.describes.map((describe) => describe.description),  description);

    services.addDescribe(food, description);
  }

  void deleteDescribe(Food food, Describe describe){
    services.deleteDescribe(food, describe);
  }

  String _checkForDuplicates(Iterable<String> items, String text)  {
    final duplicateCount = items.where((item) => item.contains(text)).length;
    if(duplicateCount > 0){
      text += '${duplicateCount + 1}';
    }
    return text;
  }
}

