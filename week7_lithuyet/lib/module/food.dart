import 'package:week7_lithuyet/module/data_layout.dart';
import '../repositories/Repository.dart';

class Food {
  int id = 0;
  String name = '';
  String images = 'assets/images/avatar_default.png';
  List<Describe> describes = [];

  Food({required id, this.name =''});

  Food.fromModel(Model model){
    id = model.id;
    name = model.data['name'] ?? '';
    images = model.data['images'] ?? '';
    if (model.data['describe'] != null){
      describes = model.data['describe'].map<Describe>((describe) => Describe.fromModel(describe)).toList();
    }
  }
  Model toModel() => Model(id: id, data: {
    'name': name,
    'describe': describes.map((describe) => describe.toModel()).toList()
  });

  int get completeCount => describes.where((describe) =>describe.complete).length;

  String get completenessMessage => '$completeCount out of ${describes.length} describes';

}