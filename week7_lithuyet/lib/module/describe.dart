import 'package:flutter/foundation.dart';
import '../repositories/Repository.dart';

class Describe{
  final int id;
  String description;
  bool complete;

  Describe({
    required this.id,
    this.complete = false,
    this.description = ''
});
  Describe.fromModel(Model model)
      : id = model.id,
        description = model.data['description'],
        complete = model.data['complete'];

  Model toModel() => Model(id: id, data: {'description': description, 'complete': complete});

}