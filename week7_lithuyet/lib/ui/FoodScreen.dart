

import 'package:flutter/material.dart';
import 'package:week7_lithuyet/module/data_layout.dart';

import '../module/food.dart';
import '../provider/FoodProvider.dart';

class FoodScreen extends StatefulWidget {
  final Food food;
  const FoodScreen({Key? key, required this.food}) : super(key: key);
  @override
  State createState() => _FoodScreenState();
}

class _FoodScreenState extends State<FoodScreen> {
  late ScrollController scrollController;
  Food get food => widget.food;
  @override
  void initState() {
    super.initState();
    scrollController = ScrollController()
      ..addListener(() {
        FocusScope.of(context).requestFocus(FocusNode());
      });
  }

  @override
  Widget build(BuildContext context) {
    // final plan = PlanProvider.of(context);
    return Scaffold(
        appBar: AppBar(title: Text('Master Plan')),
        body: Column(children: <Widget>[
          Expanded(child: _buildList()),
          SafeArea(child: Text(food.completenessMessage))
        ]),
        floatingActionButton: _buildAddTaskButton());
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  Widget _buildAddTaskButton() {
    //final plan = PlanProvider.of(context);
    return FloatingActionButton(
      child: Icon(Icons.add),
      onPressed: () {
        final controller = FoodProvider.of(context);
        controller.createDescribe(food);
        setState(() {});
      },
    );
  }

  Widget _buildList() {
    //final plan = PlanProvider.of(context);
    return ListView.builder(
      controller: scrollController,
      itemCount: food.describes.length,
      itemBuilder: (context, index) => _buildTaskTile(food.describes[index]),
    );
  }

  Widget _buildTaskTile(Describe describe) {
    return Dismissible(
      key: ValueKey(describe),
      background: Container(color: Colors.red),
      direction: DismissDirection.endToStart,
      onDismissed: (_) {
        final controller = FoodProvider.of(context);
        controller.deleteDescribe(food, describe);
        setState(() {});
      },
      child: ListTile(
        leading: Checkbox(
            value: describe.complete,
            onChanged: (selected) {
              setState(() {
               describe.complete = selected as bool;
              });
            }),
        title: TextFormField(
          initialValue: describe.description,
          onFieldSubmitted: (text) {
            setState(() {
              describe.description = text;
            });
          },
        ),
      ),
    );
  }
}
