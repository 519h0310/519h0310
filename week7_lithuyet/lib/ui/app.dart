

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:week7_lithuyet/ui/CreatorScreen.dart';

class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
      return MaterialApp(
        title: 'App Name',
        home: Scaffold(
          body: CreatorScreen(),
        ),
      );
  }

}

