import 'package:flutter/material.dart';
import 'package:week7_lithuyet/provider/FoodProvider.dart';
import 'package:week7_lithuyet/ui/FoodScreen.dart';

class CreatorScreen extends StatefulWidget{
  @override
  _CreatorScreenState createState() => _CreatorScreenState();
}

class _CreatorScreenState extends State<CreatorScreen>{

  final textController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Order food', style: TextStyle(color: Colors.white70),),
                      centerTitle: true,),
      body: Column(
        children: <Widget>[
          _buildListCreator(),
          Expanded(child: _buildOrderFoods())
        ],
      ),
    );
  }

  Widget _buildListCreator(){
    // Add Padding "Add Food"
    return Padding(
        padding: const EdgeInsets.all(25.0),
        child: Material(
          color: Theme.of(context).cardColor,
          elevation: 10,
          child: TextField(
            // controller
              controller: textController,
              decoration: InputDecoration
                (labelText: 'Add Food',
                suffixStyle: TextStyle(fontStyle: FontStyle.italic),
                contentPadding: EdgeInsets.all(25.0),

              ),
              onEditingComplete: addFood,

        ),

              ),
        );
  }

  void addFood(){
    // Get text from controller
    final text = textController.text;

    final controller = FoodProvider.of(context);
    controller.addNewFood(text);

    textController.clear();
    FocusScope.of(context).requestFocus(FocusNode());
    setState(() {});
  }

  Widget _buildOrderFoods(){
     final foods = FoodProvider.of(context).foods;

    if(foods.isEmpty){
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.note, size: 100, color: Colors.red),
          Text('You do not have any food yet.', style: Theme.of(context).textTheme.headline4,),

        ],
      );
    }

    return ListView.builder(
        itemCount: foods.length,
        itemBuilder: (context, index){
          final food = foods[index];
          return Dismissible(
              key: ValueKey(food),
              background: Container(color: Colors.red),
              direction: DismissDirection.endToStart,
              onDismissed: (_){
                final controller = FoodProvider.of(context);
                controller.deleteFood(food);
                setState(() {});
              },
              child: ListTile(
                leading: CircleAvatar(
                  backgroundImage: AssetImage(food.images),
                ),
                title: Text(food.name),
                subtitle: Text(food.completenessMessage),
                onTap: (){
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (_) => FoodScreen(food: food)));
                },
              )
          );
        }
    );
  }

}