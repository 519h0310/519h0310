import 'package:flutter/material.dart';

import '../controller/FoodController.dart';


class FoodProvider extends InheritedWidget {
  final _controller = FoodController();

  FoodProvider({Key? key, required Widget child}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;

  static FoodController of(BuildContext context) {
    FoodProvider provider = context.dependOnInheritedWidgetOfExactType<FoodProvider>() as FoodProvider;
    return provider._controller;
  }
}