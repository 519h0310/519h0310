import 'package:flutter/material.dart';
import 'package:week3_th_textstream/stream/message_stream.dart';
class App extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return TextStreamScreen();
  }
}

class TextStreamScreen extends State<App>{
  int count = 0;
  String? message;
  List<String> data = [];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Text Stream",
      home: Scaffold(
        appBar: AppBar(title: Text("App Bar"),),
        body: ListView.builder(
          itemBuilder: ( context, int index){
            return ListTile(
              trailing: Text(data[index], style: TextStyle(color: Colors.orange, fontSize: 15),),
              title: Text("List message $index", style: TextStyle(fontSize: 10.0),),
            );
          },
          itemCount: count,
        ),
        floatingActionButton: FloatingActionButton(
          child: Text("Start"),
          onPressed: (){
            addMessage();
          },
        ),
      ),
    );
  }

  addMessage() async {
    MessageStream().getMessage().listen((event) {
      setState(() {
        print(event);
        message = event;
        addData();
        count++;

      });
    });
  }

  void addData(){
    data.add(message!);
  }
}