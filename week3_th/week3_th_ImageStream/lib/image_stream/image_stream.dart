import 'package:http/http.dart' as http;
import 'dart:convert';
import '../model/imagemodel.dart';

class ImagesStream {
  Stream<ImageModel> getImage() async*{
    int count = 0;
    final List<ImageModel> listImage = [];
    fetchImage() async{
      count++;
      var url = Uri.http('jsonplaceholder.typicode.com', 'photos/$count');
      var response = await http.get(url);
      var jsonObject = json.decode(response.body);
      print(response.body);
      var image = ImageModel(jsonObject['id'],jsonObject['url']);
      listImage.add(image);
    }

    yield* Stream.periodic(Duration(seconds: 6),(int index){
      fetchImage();
      return listImage[index];
    });
  }
}
