import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:week3_imagestream/image_stream/image_stream.dart';
import 'package:week3_imagestream/model/imagemodel.dart';

class App extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return Homepage();
  }
}

class Homepage extends State<App>{
  List<ImageModel> listImage = [];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Image View",
      home: Scaffold(
        body: ListView.builder(itemCount: listImage.length,
            itemBuilder: (context, index) {
              Image.network(listImage[index].url.toString());
        }),
        floatingActionButton: FloatingActionButton(
          onPressed: getImages(),

        ),
      ),
      );
  }

  getImages() async{
    ImagesStream().getImage().listen((event) {
      listImage.add(event);
    });

    }
}
