import 'package:flutter/material.dart';

class EnglishWordsStream {
  // Stream EnglishWordsStream;

  Stream<String> getVocabulary() async* {
    final List<String> vocabulary = [
      "abandoned",
      "aberdeen",
      "abilities",
      "ability",
      "able",
      "aboriginal",
      "abortion",
      "about",
      "above",
      "abraham",
      "abroad",
      "abs",
      "absence",
      "absent",
      "absolute",
      "absolutely",
      "absorption",
      "abstract",
     " abstracts",
      "abu",
      "abuse",
      "ac",
      "academic",
      "academics",
      "academy",
      "acc",
      "accent",
      "accept",
      "acceptable",
      "acceptance",
      "accepted",
      "accepting",
      "accepts",
      "access",
      "accessed",
      "accessibility",
      "accessible",
      "accessing",
      "accessories",
      "accessory",
      "accident",
      "accidents",
      "accommodate",


    ];

    yield* Stream.periodic(Duration(seconds: 1), ( int t) {
      int index = t % 5;
      return vocabulary[index];
    });
  }
}