import 'package:flutter/material.dart';
import '../stream/english_words_stream.dart';


class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }

}

class HomePageState extends State<StatefulWidget> {
  String word = "";
  int count = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Vocabulary',
      home: Scaffold(
        appBar: AppBar(title: Text('App Bar'),),
        body: Center(child:
          Column(children: [
            const Text("Stream English word",
              style: TextStyle(color: Colors.blueGrey, fontSize: 15)),
            Text("Number of words display: " + count.toString(), style: TextStyle(color: Colors.black, fontSize: 20)),
            Text(word, style: TextStyle(color: Colors.orange, fontSize: 30),),

          ],

          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Text('Start'),
          onPressed: () {
            changeWords();
          },
        ),
      ),
    );
  }

  changeWords() async {
    EnglishWordsStream().getVocabulary().listen((eventWord) {
      setState(() {
        print(eventWord);
         word = eventWord;
         count++;
      });
    });
  }
}