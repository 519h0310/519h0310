import 'dart:async';

class Bloc{
  final streamCtl = StreamController<String>();


  receiveData(String message){
    streamCtl.sink.add(message);
  }

  Bloc(){
    streamCtl.stream.listen(
        (value){
      print(value);
    }
    );
  }
}

void main(){
  Bloc bloc1 = Bloc();

  bloc1.receiveData("Hello");

  bloc1.receiveData("Stream");


}