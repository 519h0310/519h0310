import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:week4/validate/mixin_validate.dart';


class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Fill In Information",
      home: Scaffold(
        appBar: AppBar(title: Text("Information Bar")),
        body: FillScreen(),

      )
    );

  }
  
}

class FillScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return formState();
  }

}

class formState extends State<StatefulWidget> with CommonValidation{
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        key: formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Column(
          children: [
            fieldEmailAddresss(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            fieldFirstName(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            fieldName(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            BirthYear(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            Address(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            SubmitButton()
          ],
        )
      ),
    );
    throw UnimplementedError();
  }

  Widget fieldEmailAddresss() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.email),
        labelText: "Email address"
      ),
      validator: validateEmail,
    );
  }

  Widget fieldFirstName() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
        icon: Icon(Icons.person),
        labelText: "First Name"
      ),
      validator: validateFirstName,
    );
  }

  Widget fieldName(){
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: "Last Name"
      ),
      validator: validateName,

    );
  }

  Widget BirthYear() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      keyboardType: TextInputType.datetime,
      decoration: InputDecoration(
          icon: Icon(Icons.cake),
          labelText: "Birthyear"
      ),
      validator: validateYear,
      
    );
  }

  Widget Address(){
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
        keyboardType: TextInputType.streetAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.home),
          labelText: "Address"
      ),
      validator: validateAddress,
    );
  }

  Widget SubmitButton(){
    return ElevatedButton(
        onPressed: (){
          print("Clicked to Submit button");
        },
        child: Text("Submit")
    );
  }
}

