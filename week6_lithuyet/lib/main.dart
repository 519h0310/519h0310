import 'package:flutter/material.dart';
import 'package:week6_lithuyet/ui/Chat.dart';
import 'package:week6_lithuyet/ui/StopWatchScreen.dart';
import 'package:week6_lithuyet/ui/Login.dart';


class StopwatchApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/': (context) => Login(),
        '/login': (context) => Login(),
        StopWatchScreen.route: (context) => StopWatchScreen(),
        Chat.route: (context) => Chat()
      },
      initialRoute: '/',
    );
  }

}

void main(){
  runApp(StopwatchApp());
}

