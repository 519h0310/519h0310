import 'package:flutter/material.dart';
import 'package:week6_lithuyet/ui/StopWatchScreen.dart';

import '../validate/Validator.dart';

class Login extends StatefulWidget{
  static const route = '/login';
  @override
  State<StatefulWidget> createState() {
      return LoginState();

  }
}

class LoginState extends State<StatefulWidget> with Validator{
  final formkey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Login'),),
      body: formLogin(),
    );
  }
  //Create Login form
  Widget formLogin(){
    return Form(
      key: formkey,
      child: Column(
        children: [
          emailField(),
          passwordField(),
          loginButton()
        ]
      ) ,
    );
  }

  Widget emailField(){
    return TextFormField(
      controller: emailController,
      decoration: InputDecoration(
        icon: Icon(Icons.email),
        labelText: 'Email'
      ),
      validator: EmailValidate,
    );
  }

  Widget passwordField(){
    return TextFormField(
      controller: passwordController,
      obscureText: true,
      decoration: InputDecoration(
        icon: Icon(Icons.password),
        labelText: 'Password'
      ),
      validator: PasswordValidate,
    );
  }

  Widget loginButton(){
    return ElevatedButton(
        onPressed: Validate,
        child: Text("Login"));
  }

  // Validate form and push email to StopWatch
  void Validate(){
    final form = formkey.currentState;
    if(!form!.validate()){
      return;
    }
    else{
      final email = emailController.text;
      Navigator.of(context).pushReplacementNamed(StopWatchScreen.route, arguments: email);
      setState(() {

      });
    }
  }
}