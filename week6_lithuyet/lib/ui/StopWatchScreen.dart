import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Chat.dart';
import 'Login.dart';

class StopWatchScreen extends StatefulWidget{
  static const route = '/stopwatch';
  @override
  State<StatefulWidget> createState() {
    return StopWatchState();
  }
}
class StopWatchState extends State<StatefulWidget> {

  final laps = <int>[];
  int milliseconds = 0;
  int seconds = 0;
  bool isTicking = false;
  final itemHeight = 60.0;
  final scrollController = ScrollController();
  late Timer timer;
  List<Widget> screens = [
    Chat()
  ];
  @override
  Widget build(BuildContext context) {
    // get email form Login
    String email = ModalRoute
        .of(context)!
        .settings
        .arguments as String;
    return Scaffold(
      appBar: AppBar(title: Text('Welcome $email'),),
      body: Column(
        children: [
          Expanded(child: _buildCounter(context)),
          Expanded(child: _buildLapDisplay())
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 10,
        child: Container(
          height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MaterialButton(minWidth: 45,
                        onPressed: (){
                          setState(() {
                            Navigator.of(context).pushReplacementNamed(Chat.route);
                          });
                          },
                        ),

                  ],
                )
              ],
            ),
        ),
      )

    );

  }

  Widget _buildCounter(BuildContext context) {
    return Container(
      color: Theme
          .of(context)
          .primaryColorDark,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Lap ${laps.length + 1}',
            style: Theme
                .of(context)
                .textTheme
                .subtitle1!
                .copyWith(color: Colors.white),
          ),
          Text(
              _secondsText(milliseconds),
              style: Theme.of(context)
                  .textTheme
                  .headline5!
                  .copyWith(color: Colors.white)
          ),
          SizedBox(height: 25),
          _buildControls()
        ],
      ),
    );
  }
  Row _buildControls(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(
            onPressed: isTicking ? null : _startTimer,
            child: Text('Start'),
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.blue),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white)
            ),),

        SizedBox(width:20),
        ElevatedButton(
            onPressed: isTicking ? _lap : null,
            child: Text('Lap'),
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.yellow),
              foregroundColor: MaterialStateProperty.all<Color>(Colors.black54)
            )
        ),
        SizedBox(width: 20),
        Builder(
          builder: (context) => TextButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
              foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
              ),
              child: Text('Stop'),
              onPressed: isTicking ? () => _stopTimer(context) : null,
          )
        )
      ],
    );
  }

  Widget _buildLapDisplay() {
    return Scrollbar(
        child: ListView.builder(
            controller: scrollController,
            itemExtent: itemHeight,
            itemCount: laps.length,
            itemBuilder: (context, index) {
              final milliseconds = laps[index];
              return ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 50),
                title: Text('Lap ${index + 1}'),
                trailing: Text(_secondsText(milliseconds)),
              );

            }
        )
    );
  }



  String _secondsText(int milli_seconds){
      final seconds = milli_seconds/1000;
    return '$seconds seconds';
  }

  void _startTimer() {
    timer = Timer.periodic(Duration(milliseconds: 100), _onTick);
    setState(() {
      laps.clear();
      isTicking = true;
    });
  }

  void _stopTimer(BuildContext context) {
    timer.cancel();
    setState(() {
      isTicking = false;
    });

    final controller =
    showBottomSheet(context: context, builder: _buildRunCompleteSheet);

    Future.delayed(Duration(seconds: 5)).then((_) {
      controller.close();
    });
  }

  void _onTick(Timer time) {
    setState(() {
      milliseconds += 100;
    });
  }

  void _lap() {
    scrollController.animateTo(
      itemHeight * laps.length,
      duration: Duration(milliseconds: 500),
      curve: Curves.easeIn,
    );
    setState(() {
      laps.add(milliseconds);
      milliseconds = 0;
    });
  }

  Widget _buildRunCompleteSheet(BuildContext context) {
    // final totalRuntime = laps.fold(milliseconds, (total, lap) =>
    // int.parse((total ?? '0') as String) + lap);
    int totalRuntime = laps.fold(milliseconds, (total, lap) => total + lap);
    //int totalRuntime = 0;
    laps.forEach((e) =>  totalRuntime += e);
    final textTheme = Theme.of(context).textTheme;

    return SafeArea(
        child: Container(
          color: Theme.of(context).cardColor,
          width: double.infinity,
          child: Padding(
              padding: EdgeInsets.symmetric(vertical: 30.0),
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                Text('Run Finished!', style: textTheme.headline6),
                Text('Total Run Time is ${_secondsText(totalRuntime)}.')
              ])),
        ));
  }
}

