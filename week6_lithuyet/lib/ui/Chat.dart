import 'package:flutter/material.dart';
import 'package:week6_lithuyet/stream/message_stream.dart';

import 'Login.dart';
class Chat extends StatefulWidget{
  static const route = '/chat';
  @override
  State<StatefulWidget> createState() {
    return TextStreamScreen();
  }
}

class TextStreamScreen extends State<Chat>{
  int count = 0;
  String? message;
  List<String> data = [];
  @override
  Widget build(BuildContext context) {
    addMessage();
    return MaterialApp(
      title: "Text Stream",
      home: Scaffold(
        appBar: AppBar(title: Text("App Bar"),),
        body: ListView.builder(
          itemBuilder: ( context, int index){
            return ListTile(
              trailing: Text(data[index], style: TextStyle(color: Colors.orange, fontSize: 15),),
              title: Text("List message $index", style: TextStyle(fontSize: 10.0),),
            );
          },
          itemCount: count,
        ),
        floatingActionButton: FloatingActionButton(
          child: Text("Home"),
          onPressed: (){
            setState(() {
              Navigator.of(context).pushReplacementNamed(Login.route);
            });
          },
        ),
      ),
    );
  }

  addMessage() async {
    MessageStream().getMessage().listen((event) {
      setState(() {
        print(event);
        message = event;
        addData();
        count++;

      });
    });
  }

  void addData(){
    data.add(message!);
  }
}