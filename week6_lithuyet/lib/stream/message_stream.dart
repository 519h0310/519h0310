

class MessageStream {
  Stream<String> getMessage() async*{
    final List<String> message = [
      "Hello!",
      "Hi!",
      "How are you ?",
      "I'm very poor",
      "What's up ?",
      "I lose my beautiful cat",
      "Oh! It's is bad"
    ];

    yield* Stream.periodic(Duration(seconds: 6),(int t){
      int index = t%7;
      return message[index];
    });
  }
}