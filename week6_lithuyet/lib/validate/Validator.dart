mixin Validator {
  String? EmailValidate(String? email) {
    if (email!.isEmpty) {
      return 'Email address is required.';
    }

    final regex = RegExp('[^@]+@[^\.]+\..+');
    if (!regex.hasMatch(email)) {
      return 'Enter a valid email';
    }

    return null;
  }

  String? PasswordValidate(String? password) {
    if (password!.length < 4) {
      return 'Length of password must be above 4.';
    }
  }
}
