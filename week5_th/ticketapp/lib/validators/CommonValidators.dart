

mixin CommonValidators{
  String? ValidateEmail(String? value){
    if(!RegExp(r'\S+@+\S+.\S+').hasMatch(value!)){
      return "Email invalid!";
    }
    return null;
  }

  String? ValidatePassword(String? value){
    if(!value!.isNotEmpty){
      return "Please fill in password";
    }
    if(!value[0].contains(RegExp(r'[A-Z]'))){
      return "Please password has a least 1 uppercase";
    }
  }
}