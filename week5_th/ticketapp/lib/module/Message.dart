
import 'package:ticket_app/Repository/Model.dart';
import 'data_layout.dart';

class Message {
  int id = 0;
  String username = '';
  String avatar = "assets/images/img.png";
  List<ChatMessage> chatMessages = [];

  Message({required this.id , this.username = ''});

  Message.fromModel(Model model){
    id = model.id;
    username = model.data['username'] ?? '';
    avatar = model.data['avatar'] ?? '';
    if(model.data['chatMessage'] != null){
        chatMessages = model.data['chatMessage'].map<ChatMessage>((message) => ChatMessage.fromModel(message)).toList();
    }
  }

  Model toModel() => Model(id: id, data: {
    'username': username,
    'chatMessage': chatMessages.map((message) => message.toModel()).toList()
  });

String get lastMessage => chatMessages.last.message;

}