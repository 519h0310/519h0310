


import 'package:flutter/material.dart';
import 'package:ticket_app/provider/MessageProvider.dart';
import 'package:ticket_app/view/App.dart';

void main() {
  var messProvider = MessageProvider(child: App());
  runApp(messProvider);
}


