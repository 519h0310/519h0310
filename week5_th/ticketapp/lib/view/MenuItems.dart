
import 'package:flutter/material.dart';

import '../menu/MenuItem.dart';

class MenuItems {
  static const List<MenuItem> item = [
    itemAddGroup,
    itemAddFriends,
    itemScanQR,
    itemZaloCalendar,
    itemLoginHistory,
    itemMyCloud,
    itemAddGroupCall
  ];



  static const itemAddGroup = MenuItem(
                          text: 'Tạo nhóm',
                          icon: Icons.group_add
                        );

  static const
  itemAddFriends = MenuItem(
      text: 'Thêm bạn',
      icon: Icons.person_add_alt_1
  );

  static const
  itemScanQR = MenuItem(
      text: 'Quét mã QR',
      icon: Icons.qr_code
  );

  static const
  itemZaloCalendar = MenuItem(
      text: 'Lịch Zalo',
      icon: Icons.calendar_today_outlined
  );

  static const
  itemLoginHistory = MenuItem(
      text: 'Lịch sử đăng nhập',
      icon: Icons.tv_rounded
  );

  static const
  itemMyCloud = MenuItem(
      text: 'Cloud của tôi',
      icon: Icons.cloud_queue_rounded
  );

  static const
  itemAddGroupCall = MenuItem(
      text: 'Tạo cuộc gọi nhóm',
      icon: Icons.photo_camera_front
  );
}