import 'package:flutter/material.dart';
import 'package:ticket_app/module/data_layout.dart';

class ChatScreen extends StatefulWidget{
  final Message message;
  const ChatScreen({Key? key, required this.message, }) : super (key: key);

  @override
  State<StatefulWidget> createState() =>ChatScreenState();

}

class ChatScreenState extends State<ChatScreen>{

  late ScrollController scrollController;
  Message get message => widget.message;

  @override
  void initState(){
    super.initState();
    scrollController = ScrollController()
    ..addListener(() {
      FocusScope.of(context).requestFocus(FocusNode());
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // // leading: IconButton(
        // //   icon: Icon(Icons.arrow_back),
        // //   onPressed: () { Navigator.of(context).push() },
        // ),
        title: Row(
          children:[
            CircleAvatar(backgroundImage: AssetImage(message.avatar),),
            SizedBox(width: 1,),
            Column(
              children:[
                Text(message.username, style: const TextStyle(fontSize: 20.0),),
                Text("Active", style: const TextStyle(fontSize: 10.0),),
              ]
            )
          ]
        ,
        ),
          actions: [
            IconButton(onPressed: (){}, icon: Icon(Icons.call)),
            IconButton(onPressed: (){}, icon: Icon(Icons.video_call))
        ],
      ),
      body: Body(),
    );
  }
}

class Body extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Spacer(),
        Container(
          decoration: BoxDecoration(
            color: Theme.of(context).scaffoldBackgroundColor,
          ),
          child: SafeArea(
            child: Row(
              children: [
                Icon(Icons.sticky_note_2),
                SizedBox(width: 2.0,),
                Expanded(
                    child: Container(

                      child: Row(
                        children: [

                      ],
                    ),
                ),
                    )
              ],
            ),
          ),
        )
      ],
    );
  }

}

