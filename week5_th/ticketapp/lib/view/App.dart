
import 'package:flutter/material.dart';

import '../view/HomeScreen.dart';

class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Zalo",
      theme: ThemeData(
        textTheme: TextTheme(displaySmall: TextStyle(fontSize: 5, color: Colors.grey)),
      ),
      home: HomeScreen(),
    );
  }

}

