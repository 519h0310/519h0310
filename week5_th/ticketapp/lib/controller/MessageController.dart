

import 'package:ticket_app/module/ChatMessage.dart';

import '../module/Message.dart';
import '../services/MessageService.dart';

class MessageController{
  final services = MessageService();

  List<Message> get messages => List.unmodifiable(services.getAllMessage());

  void addNewMessage(String name){
    if(name.isEmpty){
      return;
    }
    name = _checkForDuplicates(messages.map((message) => message.username), name);
    services.createMessage(name);
  }

  void saveMessage(Message message){
    services.saveMessage(message);
  }

  void deleteMessage(Message message){
    services.deleteMessage(message);
  }

  void createChat(Message message, [String? chat]){
    if (chat == null || chat.isEmpty){
      chat = 'Chào bạn mới! Chúng ta đã trở thành bạn bè của nhau!';
    }

    services.addChatMessage(message, chat);
  }

  void deleteDescribe(Message message, ChatMessage chat){
    services.deleteChatMessage(message, chat);
  }

  String _checkForDuplicates(Iterable<String> items, String text)  {
    final duplicateCount = items.where((item) => item.contains(text)).length;
    if(duplicateCount > 0){
      text += '${duplicateCount + 1}';
    }
    return text;
  }
}

