
class Model{
  final int id;
  final Map data;

  Model({ required this.id, this.data = const{}});
}
abstract class Repository{
  Model createMessage();

  List<Model> getAll();

  Model? get(int id);

  int update(Model message);

  Model? delete(int id);

  void clear();
}