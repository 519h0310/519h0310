
import 'Model.dart';

class InMemoriesCache implements Repository{
  final _storage = Map<int, Model>();
  // Clear data
  @override
  void clear() {
    _storage.clear();
  }
  //Create Model(Message)
  @override
  Model createMessage() {
    final ids = _storage.keys.toList()..sort();
    final id = (ids.isEmpty) ? 1 : ids.last + 1;

    final model = Model(id: id);
    _storage[id] = model;
    return model;
  }

  @override
  Model? delete(int id) => _storage.remove(id);

  @override
  Model? get(int id) => _storage[id];

  @override
  List<Model> getAll() => _storage.values.toList(growable: false);

  @override
  int update(Model item) {
    _storage[item.id as int] = item;

    return 0;
  }


}