// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter_test/flutter_test.dart';


import 'package:ticket_app/module/Message.dart';

import 'package:ticket_app/services/MessageService.dart';

void main() {
  test('Counter increments smoke test', ()  {
    MessageService messageServices = MessageService();

    messageServices.createMessage('ASDaasd');
    print(messageServices.getAllMessage()[0].username);
    var allMessages = messageServices.getAllMessage();

    messageServices.deleteMessage(allMessages.first);
    expect(0, allMessages.length);
  });

}
