
import 'package:flutter_test/flutter_test.dart';
import 'package:ticket_app/Repository/InMemoriesCache.dart';
import 'package:ticket_app/Repository/Model.dart';

void main() {

  test('Test InMemory Repository', (){
    // expect(repo.getAll().isEmpty, true);
    Model model = Model(id: 1);
    expect(1,model.id);

    Repository repo = InMemoriesCache();

    repo.createMessage();
    expect(1,repo.getAll().length);



  });
}
