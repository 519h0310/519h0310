import 'package:flutter/material.dart';
import 'package:week4_lithuyet/main.dart';
import 'package:week4_lithuyet/mixin/CommonValidation.dart';
class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Login page",
      home: Scaffold(
          body: LoginPage()
      ),
    );
  }
}

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }

}

class LoginState extends State<StatefulWidget> with CommonValidation{
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        child: Column(
          children: [
            emailfield(),
            Container(margin: EdgeInsets.all(20.0),),
            passwordfield(),

          ],
        ),
      ),
    );
  }

Widget emailfield(){
  return TextFormField(
    keyboardType: TextInputType.emailAddress,
    autovalidateMode: AutovalidateMode.onUserInteraction,
    decoration: InputDecoration(
      icon: Icon(Icons.alternate_email),
      labelText: 'Email Adress'
    ),
    validator: validateEmail,
  );
}

  Widget passwordfield(){
    return TextFormField(
        obscureText: true,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        decoration: InputDecoration(
          icon: Icon(Icons.password),
          labelText: 'Password'
      ),
      validator: validatePassword,
    );
  }

}