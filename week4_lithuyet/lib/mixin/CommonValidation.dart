mixin CommonValidation{

  String? validateEmail(String? value) {
    if (!RegExp(r'\S+@\S+\.\S+').hasMatch(value!)) {
      return "Email invalid.";
    }
    return null;
  }

  String? validatePassword(String? value) {
    if (value!.isEmpty) {
      return "Please fill in field!";
    }
    if (value!.trim().length < 8) {
      return "Passwords has at least 8 characters";
    }
    else {
      if (!value!.contains(new RegExp(r'[!@#$%^&*(),.?":{}|<>]'))) {
        return "Passwords at least 1 special character";
      }
      if (!value!.contains(new RegExp(r'[A-Z]'))) {
        return "Passwords at least 1 uppercase characters";
      }
      if (!value!.contains(new RegExp(r'[A-Z]'))) {
        return "Passwords at least 1 uppercase characters";
      }
      if (!value!.contains(new RegExp(r'[a-z]'))) {
        return "Passwords at least 1 normal characters";
      }
      return null;
    }
  }
}
