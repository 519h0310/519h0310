import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:week1_th/module/Image_module.dart';
import 'dart:convert';


class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }

}

class AppState extends State<App> {
  int counter = 0;
  List<ImageModel> listImage = [];
  fetchImage() async {
    counter ++;
    var url = Uri.http('jsonplaceholder.typicode.com', 'photos/$counter');
    var response = await http.get(url);

    print(response.body);

    var jsonObject = json.decode(response.body);
    var imageModule = ImageModel(jsonObject['id'], jsonObject['url']);
    listImage.add(imageModule);

    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    var appWidget = new MaterialApp(
        home: Scaffold(
          appBar: AppBar(title: Text('Image Viewer'),),
          body: ListView.builder(
            itemCount: listImage.length,
            itemBuilder:(BuildContext context, int index){
              return Image.network(listImage[index].url.toString());
            },

          ),
          floatingActionButton: FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: fetchImage
          ),
        )
    );

    return appWidget;
  }

}