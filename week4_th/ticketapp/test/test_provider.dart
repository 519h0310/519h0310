
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:ticket_app/provider/MessageProvider.dart';


void main(){
  test('Test Providẻr', (){
    runApp(CreatorScreen());
    
  });
  
  test('Test message', (){

  });
}

class CreatorScreen extends StatefulWidget{
  @override
  _CreatorScreenState createState() => _CreatorScreenState();
}

class _CreatorScreenState extends State<CreatorScreen> {

  final textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Order Message', style: TextStyle(color: Colors.white70),),
        centerTitle: true,),
      body: Column(
        children: <Widget>[
          _buildListCreator(),
          Expanded(child: _buildOrderMessages())
        ],
      ),
    );
  }
  Widget _buildListCreator(){
    // Add Padding "Add Message"
    return Padding(
      padding: const EdgeInsets.all(25.0),
      child: Material(
        color: Theme.of(context).cardColor,
        elevation: 10,
        child: TextField(
          // controller
          controller: textController,
          decoration: InputDecoration
            (labelText: 'Add Message',
            suffixStyle: TextStyle(fontStyle: FontStyle.italic),
            contentPadding: EdgeInsets.all(25.0),

          ),
          onEditingComplete: addMessage,

        ),

      ),
    );
  }
  void addMessage(){
    // Get text from controller
    final text = textController.text;

    final controller = MessageProvider.of(context);
    controller.addNewMessage(text);

    textController.clear();
    FocusScope.of(context).requestFocus(FocusNode());
    setState(() {});
  }

  Widget _buildOrderMessages() {
    final messages = MessageProvider
        .of(context)
        .messages;

    if(messages.isEmpty){
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.note, size: 100, color: Colors.red),
          Text('You do not have any Message yet.', style: Theme.of(context).textTheme.headline4,),
        ],
      );
    }

    return ListView.builder(
        itemCount: messages.length,
        itemBuilder: (context, index){
          final message = messages[index];
          return Dismissible(
              key: ValueKey(message),
              background: Container(color: Colors.red),
              direction: DismissDirection.endToStart,
              onDismissed: (_){
                final controller = MessageProvider.of(context);
                controller.deleteMessage(message);
                setState(() {});
              },
              child: ListTile(
                title: Text(message.username),
                subtitle: Text(message.lastMessage),
                onTap: (){

                },
              )
          );
        }
    );
  }
}





