

import '../Repository/InMemoriesCache.dart';
import '../Repository/Model.dart';
import '../module/ChatMessage.dart';
import '../module/Message.dart';

class MessageService{
  final Repository _repository = InMemoriesCache();

  Message createMessage(String username){
    final model = _repository.createMessage();
    final message = Message.fromModel(model)..username = username;
    saveMessage(message);
    return message;
  }

  void saveMessage(Message message){
    _repository.update(message.toModel());
  }

  void deleteMessage(Message message){
    _repository.delete(message.toModel().id);
  }

  List<Message> getAllMessage(){
    return _repository
        .getAll()
        .map<Message>((model) => Message.fromModel(model))
        .toList();
  }

  void addChatMessage(Message message, String chat){
    print('Message.ChatMessage = ${message.chatMessages}');
    print('Message.ChatMessage.last = ${message.chatMessages.length}');

    final id = (message.chatMessages.length > 0) ? message.chatMessages.last.id : 1;

    final chatMessage = ChatMessage(id: id, message: chat);
    message.chatMessages.add(chatMessage);
    saveMessage(message);
  }

  void deleteChatMessage(Message message, ChatMessage chat){
    message.chatMessages.remove(chat);
    saveMessage(message);
  }
}