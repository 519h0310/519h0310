

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ticket_app/controller/MessageController.dart';
import 'package:ticket_app/menu/MenuItem.dart';
import 'package:ticket_app/view/MenuItems.dart';
import '../provider/MessageProvider.dart';
import 'ChatScreen.dart';

class HomeScreen extends StatefulWidget{


  @override
  State<StatefulWidget> createState() {
    return HomeState();
  }
}

class HomeState extends State<HomeScreen>{
  int current_index = 0;
  final textController = TextEditingController();
  @override
  Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBarDesign(),
        body: MessPage(),
        bottomNavigationBar: BottomNavigationBar(
            currentIndex: current_index,
            type: BottomNavigationBarType.fixed,
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.message_rounded, color: Colors.grey,),
                label: 'Tin nhắn',
                activeIcon: Icon(Icons.message_rounded, color: Colors.blue)
              ),

              BottomNavigationBarItem(
                  icon: Icon(Icons.person_pin_outlined, color: Colors.grey),
                  label: 'Danh bạ',
                  activeIcon: Icon(Icons.person_pin_outlined, color: Colors.blue)
              ),

              BottomNavigationBarItem(
                  icon: Icon(Icons.apps_sharp, color: Colors.grey),
                  label: 'Khám phá',
                  activeIcon: Icon(Icons.apps_sharp, color: Colors.blue)
              ),

              BottomNavigationBarItem(
                  icon: Icon(Icons.watch_later_outlined, color: Colors.grey),
                  label: 'Nhật ký',
                  activeIcon: Icon(Icons.watch_later_outlined, color: Colors.blue)
                  ),

              BottomNavigationBarItem(
                  icon: Icon(Icons.person_outline, color: Colors.grey),
                  label: 'Cá nhân',
                  activeIcon: Icon(Icons.person_outline, color: Colors.blue),

              ),
            ],
            onTap: (index ){
              setState(() {
                current_index = index;
              });
            }
        ),


      );
    }
  Widget MessPage() {
    final messages = MessageProvider.of(context).messages;
     if(messages.length == 0){
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.search, size: 100, color: Colors.blue),
          Text(messages.length.toString()),
          Text('You do not have any Messages yet.', style: Theme.of(context).textTheme.headline2,),

        ],
      );
     }

    return ListView.builder(
        itemCount: messages.length,
        itemBuilder: (context, index){
          final message = messages[index];
          final controller = MessageProvider.of(context);
          controller.createChat(message);
          return Dismissible(
            key: ValueKey(message),
            direction: DismissDirection.endToStart,
            onDismissed: (_){
              final controller = MessageProvider.of(context);
              controller.deleteMessage(message);
              setState(() {});
              },
            background: Container(
              child: Row(
                children: <Widget>[
                  IconButton(onPressed:(){
                    final controller = MessageProvider.of(this.context);
                    controller.deleteMessage(message);

                    setState((){});
                  }, icon: Icon(Icons.restore_from_trash), color: Colors.red,)
                ],
              ),
            ),
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage("img.png"),
              ),
              title: Text(message.username.toString()),
              subtitle: Text(message.lastMessage.toString()),
              onTap:() {
                  Navigator.of(context).push(MaterialPageRoute(builder: (_) => ChatScreen(message : message)));
              },
            ),
          );
        }
    );
  }

    PreferredSizeWidget AppBarDesign(){
      return AppBar(
          title: TextField(
            controller: textController,
            decoration: InputDecoration(
              hintText: 'Tìm bạn bè , tin nhắn...',
              hintStyle: TextStyle(color: Colors.white, fontSize: 20.0),
            ),
            onEditingComplete: addMessage,
          ),
          elevation: 0,
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [ Colors.blueAccent,
                      Colors.blue,
                      Colors.lightBlue,
                      Colors.lightBlueAccent])
            ),
          ),
          actions: [
            Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: IconButton(onPressed: OpenQRCode, icon: Icon(Icons.qr_code_scanner)),
            ),

            Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: PopupMenuButton<MenuItem>(
                icon: Icon(Icons.add),
                iconSize: 35,
                onSelected:(item) => onSelected(context,item),
                itemBuilder: (context) => [
                  ...MenuItems.item.map((buildItems)).toList(),          ],
              ) ,
            )
          ],
          leading: IconButton(onPressed: OpenQRCode, icon: Icon(Icons.search, size: 35,))
      );

    }

    PopupMenuItem<MenuItem> buildItems(MenuItem item) => PopupMenuItem(
        child: Container(
          child:
          Row(

            children: [
              Icon(item.icon, color: Colors.grey),
              const SizedBox(width: 10),
              Text(item.text)
            ],
          ),
        ),
        value: item
    );

    void OpenQRCode() {

    }

    void addMessage(){
        // Get text from controller
        final text = textController.text;
        final controller = MessageProvider.of(context);
        controller.addNewMessage(text);
        textController.clear();
        FocusScope.of(context).requestFocus(FocusNode());
        setState(() {

        });
    }


    void onSelected(BuildContext context,MenuItem item) {

    }


  }



