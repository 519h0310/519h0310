
import 'package:flutter/material.dart';

import '../controller/MessageController.dart';

class MessageProvider extends InheritedWidget {
  final _controller = MessageController();

  MessageProvider({Key? key, required Widget child}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;

  static MessageController of(BuildContext context) {
    MessageProvider provider = context.dependOnInheritedWidgetOfExactType<MessageProvider>() as MessageProvider;
    return provider._controller;
  }
}