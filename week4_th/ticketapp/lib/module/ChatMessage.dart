import '../Repository/Model.dart';

class ChatMessage{
  final int id;
  String message;
  bool seen;

  ChatMessage(
      {
        required this.id,
        this.message = '',
        this.seen = false
      });

  ChatMessage.fromModel(Model model)
      : id = model.id,
      message = model.data['message'],
      seen = model.data['seen'];

  Model toModel() => Model(id: id, data: {'message': message, 'seen': seen});


}